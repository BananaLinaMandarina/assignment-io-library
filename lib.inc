section .text
exit: 
    mov rax, 60
    syscall
    ret 

string_length:
    xor rax, rax
    .cycle:
        cmp byte[rdi+rax], 0
        je .end
        inc rax
        jmp .cycle
    .end:
        ret


print_string:
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
    ret


print_newline:
    mov rdi, `\n` 
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret


print_uint:
    mov rax, rdi
    mov r8, 10
    xor r9, r9
    push r9
    .cycle:
        xor rdx, rdx
        div r8
        add rdx, '0'
        dec rsp
        mov byte[rsp], dl
        inc r9
        cmp rax, 0
        je .print_number
        jmp .cycle
    .print_number:
        mov rdi, rsp
        call print_string
        add rsp, 8
        add rsp, r9   
        ret

print_int:
    mov r8, rdi
    cmp rdi, 0
    jl .minus
    jmp print_uint
    .minus:
        mov rdi, '-'
        call print_char
        neg r8
        mov rdi, r8
        call print_uint
    .end:
        ret


string_equals:
    .A:
        mov al, byte[rsi]
        cmp byte[rdi], al
        je .is_zero
        jmp .B
        .is_zero:
            cmp byte[rdi], 0
            je .C
            inc rdi
            inc rsi
            jmp .A
    .B:
        xor rax, rax
        ret
    .C:
        mov rax, 1
        ret


read_char:
    dec rsp
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    cmp rax, 0
    je .end
    mov al, byte[rsp]
    inc rsp
    ret
    .end:
        inc rsp
        ret
    

read_word:
    xor rdx, rdx
    .cycle:
        push rdx
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        pop rdx
        cmp rax, 0
        je .yes
        cmp rdx, 0
        jne .next
        cmp rax, `\n`
        je .cycle
        cmp rax, `\t`
        je .cycle
        cmp rax, ` `
        je .cycle
    .next:
        cmp rdx, rsi
        je .no
        cmp rax, `\n`
        je .yes
        cmp rax, `\t`
        je .yes
        cmp rax, ` `
        je .yes
        mov r8, rax
        mov byte[rdx+rdi], r8b
        inc rdx
        jmp .cycle
    .yes:
        mov byte[rdx+rdi], 0
        mov rax, rdi
        ret
    .no:
        mov rax, 0
        ret


parse_uint:
    xor rax, rax
    xor rdx, rdx
    .cycle:
        xor r8, r8
        mov r8b, byte[rdi+rdx]
        cmp r8b, 0
        je .end
        cmp r8b, '0'
        jl .end
        cmp r8b, '9'
        jg .end
        sub r8b, '0'
        push rdx
        mov r9, 10
        mul r9
        pop rdx
        add rax, r8    
        inc rdx
        jmp .cycle
    .end:
        ret



parse_int:
    xor rdx, rdx
    cmp byte[rdi], '-'
    je .minus
    jmp parse_uint
    .minus:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
    .end:
        ret


string_copy:
    xor rax, rax
    .cycle:
        cmp byte[rdi+rax], 0
        je .ok
        cmp rax, rdx
        je .not_ok
        mov r8b, byte[rdi+rax]
        mov byte[rsi+rax], r8b
        inc rax
        jmp .cycle
    .ok:
        mov byte[rsi+rax], 0
        ret
    .not_ok:
        mov byte[rdi+rax], 0
        xor rax, rax
        ret





















